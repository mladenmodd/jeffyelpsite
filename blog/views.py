from django.shortcuts import render

# Create your views here.
def index(request):
    template_name ='blog/index.html'
    return render(request, template_name)

def services(request):
    template_name = 'blog/services.html'

    return render(request, template_name)


def blog(request):
    template_name = 'blog/blog.html'

    return render(request, template_name)


def portfolio(request):
    template_name = 'blog/portfolio.html'

    return render(request, template_name)


def projects(request):
    template_name = 'blog/projects.html'

    return render(request, template_name)


def hireme(request):
    template_name = 'blog/hireme.html'

    return render(request, template_name)


def contact(request):
    template_name = 'blog/contact.html'

    return render(request, template_name)

