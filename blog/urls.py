from django.urls import path, include
from . import views
import logging
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView





urlpatterns = [
    path('', views.index, name='index'),
    path('hireme', views.hireme, name='hireme'),
    path('blog', views.blog, name='blog'),
    path('portfolio', views.portfolio, name='portfolio'),
    path('projects', views.projects, name='projects'),
    path('services', views.services, name='services'),
    path('contact', views.contact, name='contact'),

]
