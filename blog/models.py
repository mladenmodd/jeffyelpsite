from django.db import models

# Create your models here.


class gallery_post(models.Model):
    meta = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    date = models.DateField()
    thumbnail_img = models.ImageField()
    full_img = models.ImageField()


class blog_post(models.Model):
    meta = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    body = models.TextField()
    date = models.DateField()
    thumbnail_img = models.ImageField()
    full_img = models.ImageField()


class projects_post(models.Model):
    meta = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    body = models.CharField(max_length=255)
    date = models.DateField()
    thumbnail_img = models.ImageField()
    full_img = models.ImageField()




