"""views.py: views for website"""
"""last commit: 08012019 12:43 am"""

from django.urls import path, include
from . import views
import logging
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView



logger = logging.getLogger(__name__)


urlpatterns = [
    path('jeff', views.index, name='index'),
    path('jeff/history', views.history, name='history'),
    path('login', LoginView.as_view(), name='login'),
    path('jeff/search',  views.search.as_view(), name='search'),
    path('jeff/download/<str:uid>', views.dwn_csv, name='download'),
    path('jeff/table/<str:uid>', views.table, name='table'),
    path('jeff/deepsearch/<str:uid>', views.deep_search, name='deepsearch'),
    path('jeff/status', views.status, name='status'),
    path('jeff/upload_csv', views.upload_csv, name='upload_csv'),
    path('jeff/master', views.master, name='master'),
    path('jeff/states/<str:state>', views.tablestate, name='tablestate'),
    path('jeff/term/<str:term>', views.tableterm, name='tableterm'),
    path('jeff/states', views.state, name='states'),
    path('jeff/terms', views.term, name='terms'),
    path('jeff/cities', views.cities, name='cities'),
    path('jeff/cities/<str:state>/<str:city>', views.city, name='city'),
]
