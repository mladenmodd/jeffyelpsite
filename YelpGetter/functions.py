#!/usr/bin/env python

"""functions.py: functions for getting results out of yelp"""
"""last commit: 08012019 02:08 am"""


import datetime
import json
import csv
import requests
from django.db import connection
from threading import Thread
from YelpGetter.models  import Search, Result, ApiData
import logging
from requests_html import HTMLSession
import time
import re



logger = logging.getLogger(__name__)

def convert_field(field):
    try:
      return field.replace('"', '')
    except:
      return ''

def getSheetData(fields):
  try:
    name = functions.convert_field(fields[0])
  except:
    name = ''

  try:
    yurl = functions.convert_field(fields[2])
  except:
    yurl = ''

  try:
    address = functions.convert_field(fields[4]),
  except:
    address = ''

  try:
    surl = functions.convert_field(fields[3])
  except:
    surl = ''

  try:
    city = functions.convert_field(fields[5])
  except:
    city = ''

  try:
    state = functions.convert_field(fields[6])
  except:
    state = ''
  try:
    phone = "+1" + functions.convert_field(fields[2].replace('(', "").replace(')', "").replace(' ', "").replace('-', '').replace('"',''))
  except:
    phone= ''

  return {'name' : name,
          'yurl' : name,
          'address' : address,
          'surl' : surl,
          'city' : city,
          'state' : state,
          'phone' : phone,
          }

def start_new_thread(function):
    def decorator(*args, **kwargs):
        t = Thread(target = function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator


@start_new_thread
def scrape2(term, location):
  import django
  django.setup()
  now = datetime.datetime.now()
  def  cat(cats):
    catz = ''
    for item in cats:
      catz = catz + '|' + item['title']
    return catz

  def dateGen(now):
    return str(now.year) + str(now.day) + str(now.month) + '_' + str(now.hour) + str(now.minute)
  
  def uidGen(now):
    return str(now.hour) + str(now.minute) + str(now.second) + str(now.year) + str(now.day) + str(now.month)# #str()

  MY_API_KEY = "1S4HSe-tv0jePN4ytkR-6NLHx9zCRAEv9r3EU0pYeT1QzXGayzoIvOU2SGLLCkM5e9Rbm9L9AS6HhbbPe4feLFuBWvW8t6DvDObmgYKoHuN_h9pMVlp_J9WmvgnbXHYx"
  offsets = [x for x in range(0,1000,50)]
  headers = {'Authorization': 'Bearer %s' % MY_API_KEY}
  #try:
  uid = uidGen(now)
  searchDB = Search(term = term,location = location,date = now,uid = uid,)
  searchDB.save()
  for x in offsets:
    url='https://api.yelp.com/v3/businesses/search'
    params = {'term': term, 'location': location, 'offset': x}

    with open('log.txt','a') as f:
        f.write('url ' + str(x) + '\n')

    req = requests.get(url, headers=headers, params=params)

    parsed = json.loads(req.text)

    with open('log.txt','a') as f:
      f.write('started \n')

    for item in parsed['businesses']:
      hurl = 'www.' + ''.join(item['name'].split()) + '.com'
      hurl = hurl.replace('-', '').replace('&', 'and').replace("'", '').lower()
      req2 = requests.get('https://api.yelp.com/v3/businesses/'+item['id'], headers=headers, params=params)
      parsed2 = json.loads(req2.text)
      yelp_url = item['url'].split('?')

      if (Result.objects.filter(yurl=yelp_url[0]).count()  != 0) or (Result.objects.filter(phone=item['phone']).count() != 0):
          pass
      else:
        resultDB = Result(
                   uid = uid,
                   name = item['name'],
                   yurl = yelp_url[0],
                   review_count = item['review_count'],
                   address = item["location"]["address1"],
                   city =  item['location']['city'],
                   state = item['location']['state'],
                   categories = cat(item['categories']),
                   phone = item['phone'],
                   surl = hurl,
                   rating = item['rating'],
                   is_claimed = str(parsed2['is_claimed']),
                   term = term
                  )
        try:
          resultDB.save()
        except:
          pass

@start_new_thread
def deepscrape(uid):
  import django
  django.setup()
  email_rex = '(\w+[.|\w])*@(\w+[.])*\w+'
  session = HTMLSession()
  results = Result.objects.filter(uid=uid)
  for result in results:
    r = session.get(result.yurl)
    time.sleep(2)
    url = r.html.find('.biz-website.js-biz-website.js-add-url-tagging a', first=True)
    try:
      r = session.get('http://www.' + str(url.text))    
      result.surl = 'http://www.' + str(url.text)
      print('http://www.' + str(url.text))
      mail = r.html.search("mailto:{}@{}.{}{}{}")
      if mail is not None:
        result.mail = mail[0]+'@'+mail[1]+'.'+mail[2]+mail[3]+mail[4]
      result.save()
    except Exception as e:
      pass

  
 





#todo: Master DB for uploading to the sheets
#todo: DB for storing last item uploaded to the shets.

#https://www.yelp.com/biz/boutique-bites-chicago?adjust_creative=K9jLxBTH0ipsFvWsdAEA3g&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=K9jLxBTH0ipsFvWsdAEA3g








