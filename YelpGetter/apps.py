from django.apps import AppConfig


class YelpgetterConfig(AppConfig):
    name = 'YelpGetter'
